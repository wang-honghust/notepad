English | [简体中文](./README.md)
# notepad

#### presentation
Windows notepad imitation using QT

#### showcase
- Home screen
![Home screen](img/Snipaste_2023-07-11_17-59-40.png)
- Find
![Find](img/Snipaste_2023-07-11_18-00-19.png)
- Replace
![Replace](img/Snipaste_2023-07-11_18-00-29.png)
- Print
![Print](img/Snipaste_2023-07-11_18-01-02.png)
- Font
![Font](img/Snipaste_2023-07-11_18-01-18.png)