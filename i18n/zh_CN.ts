<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en_US">
<context>
    <name>FindDlg</name>
    <message>
        <location filename="../finddlg.ui" line="26"/>
        <source>Find</source>
        <translation type="unfinished">查找(F)</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="41"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="48"/>
        <source>Find next(F)</source>
        <translation>查找下一个(F)</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="58"/>
        <source>Find content(N):</source>
        <translation>查找内容(N):</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="74"/>
        <source>Direction</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="89"/>
        <source>Down(D)</source>
        <translation>向下(D)</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="82"/>
        <source>Up(U)</source>
        <translation>向上(U)</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="110"/>
        <source>Case-sensitive(C)</source>
        <translation>区分大小写(C)</translation>
    </message>
    <message>
        <location filename="../finddlg.ui" line="117"/>
        <source>Cycle(R)</source>
        <translation>循环(R)</translation>
    </message>
</context>
<context>
    <name>NotePad</name>
    <message>
        <location filename="../notepad.ui" line="14"/>
        <location filename="../notepad.cpp" line="474"/>
        <location filename="../notepad.cpp" line="515"/>
        <location filename="../notepad.cpp" line="518"/>
        <location filename="../notepad.h" line="80"/>
        <source>NotePad</source>
        <translation>记事本</translation>
    </message>
    <message>
        <source>文件(F)</source>
        <translation type="obsolete">文件</translation>
    </message>
    <message>
        <source>编辑(E)</source>
        <translation type="obsolete">编辑(E)</translation>
    </message>
    <message>
        <source>格式(O)</source>
        <translation type="obsolete">格式(O)</translation>
    </message>
    <message>
        <source>查看(V)</source>
        <translation type="obsolete">查看(V)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="121"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="77"/>
        <source>File(F)</source>
        <translation>文件(F)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="93"/>
        <source>Edit(E)</source>
        <translation>编辑(E)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="110"/>
        <source>Format(O)</source>
        <translation>格式(O)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="117"/>
        <source>View(V)</source>
        <translation>查看(V)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="132"/>
        <source>Help(H)</source>
        <translation>帮助(H)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="148"/>
        <source>New</source>
        <translation>新建(N)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="151"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="156"/>
        <source>Open</source>
        <translation>打开(O)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="159"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="164"/>
        <source>Save</source>
        <translation>保存(S)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="167"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="172"/>
        <location filename="../notepad.cpp" line="252"/>
        <source>Save as</source>
        <translation>另存为(A)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="175"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="180"/>
        <source>Undo</source>
        <translation>撤销(U)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="183"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="188"/>
        <source>Cut</source>
        <translation>剪切(T)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="191"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="196"/>
        <source>Copy</source>
        <translation>复制(C)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="199"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="204"/>
        <source>Paste</source>
        <translation>粘贴(P)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="207"/>
        <source>Ctrl+V</source>
        <translation>粘贴(P)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="212"/>
        <source>Delete</source>
        <translation>删除(L)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="215"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="220"/>
        <source>Auto line</source>
        <translation>自动换行</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="225"/>
        <source>Font</source>
        <translation>字体(F)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="230"/>
        <source>Status bar(S)</source>
        <translation>状态栏(S)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="235"/>
        <source>View help(H)</source>
        <translation>查看帮助(H)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="240"/>
        <source>Send feedback(F)</source>
        <translation>发送反馈(F)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="245"/>
        <source>About notepad(A)</source>
        <translation>关于记事本(A)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="250"/>
        <source>Zoom in</source>
        <translation>放大(I)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="253"/>
        <source>Ctrl++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="258"/>
        <source>Zoom out</source>
        <translation>缩小(O)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="261"/>
        <source>Ctrl+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="266"/>
        <source>Restore the default zoom</source>
        <translation>恢复默认缩放</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="271"/>
        <source>Page setting</source>
        <translation>页面设置</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="276"/>
        <location filename="../notepad.ui" line="312"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="281"/>
        <source>Find</source>
        <translation>查找(F)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="284"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="289"/>
        <source>Find next</source>
        <translation>查找下一个</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="294"/>
        <source>Find Previous</source>
        <translation>查找上一个</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="299"/>
        <source>Replace</source>
        <translation>替换(R)</translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="302"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.ui" line="307"/>
        <source>Print(P)</source>
        <translation>打印(P)</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="141"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="141"/>
        <source>*.txt ;; All file (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="93"/>
        <location filename="../notepad.cpp" line="354"/>
        <source>line</source>
        <translation>行</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="192"/>
        <source>Do you want to save your changes to</source>
        <translation>你想将更改保存到</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="192"/>
        <source>?</source>
        <translation>吗?</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="198"/>
        <source>Save(Y)</source>
        <translation>保存(Y)</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="200"/>
        <source>Dont&apos;t save(N)</source>
        <translation type="unfinished">不保存(N)</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="474"/>
        <source>Operation has been canceled by the user.</source>
        <translation>操作已被用户取消</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="515"/>
        <location filename="../notepad.cpp" line="518"/>
        <source>Can not find</source>
        <translation>找不到</translation>
    </message>
    <message>
        <source>Save </source>
        <translation type="vanished">保存(Y)</translation>
    </message>
    <message>
        <source>Dont&apos;t save</source>
        <translation type="vanished">不保存(N)</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="202"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="252"/>
        <source>Text file</source>
        <translation>文本文件</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="354"/>
        <source>,</source>
        <translation>，</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="93"/>
        <location filename="../notepad.cpp" line="354"/>
        <source>column</source>
        <translation>列</translation>
    </message>
    <message>
        <location filename="../notepad.cpp" line="125"/>
        <location filename="../notepad.h" line="81"/>
        <source>untitled</source>
        <translation>无标题</translation>
    </message>
</context>
<context>
    <name>Replace</name>
    <message>
        <location filename="../replace.ui" line="26"/>
        <source>Replace</source>
        <translation type="unfinished">替换(R)</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="41"/>
        <source>Case-sensitive(C)</source>
        <translation type="unfinished">区分大小写(C)</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="48"/>
        <source>Cycle(R)</source>
        <translation type="unfinished">循环(R)</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="69"/>
        <source>Replace(R)</source>
        <translation>替换(R)</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="76"/>
        <source>Replace content(N):</source>
        <translation>替换内容(N):</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="83"/>
        <source>Replace all(A)</source>
        <translation>替换全部(A)</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="93"/>
        <source>Find next(F)</source>
        <translation type="unfinished">查找下一个(F)</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="100"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../replace.ui" line="107"/>
        <source>Replace as(P):</source>
        <translation>替换为(P):</translation>
    </message>
</context>
</TS>
