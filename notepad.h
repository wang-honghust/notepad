
#ifndef NOTEPAD_H
#define NOTEPAD_H

#include <QMainWindow>
#include <QMessageBox>
#include <QCloseEvent>
#include <QLabel>
#include "replace.h"
#include "finddlg.h"


QT_BEGIN_NAMESPACE
namespace Ui { class NotePad; }
QT_END_NAMESPACE

class NotePad : public QMainWindow

{
    Q_OBJECT

public:
    NotePad(QWidget *parent = nullptr);
    ~NotePad();
    bool m_edited = false;
    bool inline save();
    QMessageBox::StandardButton showSaveDialog();
    void loadStyle(const QString &qssFile);
    void selectWord(const QString &word, int index ,bool down);

protected:
    void closeEvent(QCloseEvent *event) override;

public slots:
    void newFile();
    void openFile();
    bool saveFile();
    bool saveAs();
    void textEdited();
    void setFindReplaceDlg();
    void setEdited(bool newValue)
    {
        if (m_edited != newValue)
        {
            m_edited = newValue;
            emit editedChanged(m_edited);
        }
    }
    void updateTitle(bool edited);
    void undo();
    void copy();
    void cut();
    void paste();
    void editAble();
    void undoAble();
    void cursorChanged();
    void scaleChanged();
    void del();
    void setFont(QFont size);
    void fontScaleDown();
    void fontScaleUp();
    void setAutoWrap();
    void selectFont();
    void showStatusBar();
    void setDefaultZoom();
    void showReplaceDlg();
    void showFindDlg();
    void find(const QString &text,bool down,bool caseSensitive,bool cycle);
    void findNext();
    void findPrev();
    void replace(const QString &text,const QString &replaceText,bool down,bool caseSensitive,bool cycle,bool all);
    void print();


signals:
    void editedChanged(bool newValue);

private:
    Ui::NotePad *ui;
    const QString m_appName = QString("%1").arg(tr("NotePad"));
    QString m_filePath = QString("%1.txt").arg(tr("untitled"));
    QLabel *m_cursorText;
    QLabel *m_scale;
    bool m_autoWrap = true;
    QFont m_font;
    bool m_statusBar = true;
    Replace *m_replace;
    FindDlg *m_find;
    QString m_findText;
    QString m_replaceText;
    bool m_findDirection = true; // means down
    int m_currentIndex = 0; // current index of word search

};

#endif // NOTEPAD_H
