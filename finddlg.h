#ifndef FINDDLG_H
#define FINDDLG_H

#include <QDialog>

namespace Ui {
class FindDlg;
}

class FindDlg : public QDialog
{
    Q_OBJECT

public:
    explicit FindDlg(QWidget *parent = nullptr);
    ~FindDlg();
    void setContent(QString text){
        content = text;
    };
    void setFindText(QString text);

private slots:
    void on_pushButton_2_clicked();
    void findNext();

    void on_findText_textChanged(const QString &arg1);

signals:
    void valueChanged(QString text,bool down,bool caseSensitive,bool cycle);

private:
    Ui::FindDlg *ui;
    QString content;
    QString findText;
};

#endif // FINDDLG_H
