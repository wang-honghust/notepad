#include "replace.h"
#include "ui_replace.h"

Replace::Replace(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Replace)
{
    ui->setupUi(this);

}

Replace::~Replace()
{
    delete ui;
}

void Replace::on_pushButton_4_clicked()
{
    hide();
}

void Replace::setReplaceText(const QString &text){
    this->text = text;

    on_replaceText_textChanged(text);

    ui->replaceText->setText(this->text);
    ui->replaceText->setFocus();
    ui->replaceText->selectAll();
}

void Replace::on_replaceText_textChanged(const QString &arg1)
{
    if(arg1.isEmpty()){
        ui->findNextBtn->setDisabled(true);
        ui->replaceBtn->setDisabled(true);
        ui->replaceAllBtn->setDisabled(true);
    }else{
        ui->findNextBtn->setDisabled(false);
        ui->replaceBtn->setDisabled(false);
        ui->replaceAllBtn->setDisabled(false);
    }
}


void Replace::on_findNextBtn_clicked()
{
    text = ui->replaceText->text();
    emit valueChanged(text,true,ui->caseSensitive->isChecked(),ui->cycle->isChecked());
}


void Replace::on_replaceBtn_clicked()
{
    text = ui->replaceText->text();
    QString replaceAsText = ui->replaceAsText->text();
    emit replace(text,replaceAsText,true,ui->caseSensitive->isChecked(),ui->cycle->isChecked(),false);
}


void Replace::on_replaceAllBtn_clicked()
{
    text = ui->replaceText->text();
    QString replaceAsText = ui->replaceAsText->text();
    emit replace(text,replaceAsText,true,ui->caseSensitive->isChecked(),ui->cycle->isChecked(),true);
}

