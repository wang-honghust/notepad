
#include "notepad.h"
#include "./ui_notepad.h"
#include <QFileDialog>
#include <QFile>
#include <QPushButton>
#include <QClipboard>
#include <QFontDialog>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>


NotePad::NotePad(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::NotePad)
{
    m_replace = new Replace(this);
    m_find = new FindDlg(this);

    connect(m_find, &FindDlg::valueChanged, this, &NotePad::find);
    connect(m_replace, &Replace::valueChanged, this, &NotePad::find);
    connect(m_replace, &Replace::replace, this, &NotePad::replace);

    ui->setupUi(this);
    ui->text->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->text->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    connect(ui->actionNew, &QAction::triggered, this, &NotePad::newFile);
    connect(ui->actionopen, &QAction::triggered, this, &NotePad::openFile);
    connect(ui->actionSave, &QAction::triggered, this, &NotePad::saveFile);
    connect(ui->actionSave_as, &QAction::triggered, this, &NotePad::saveAs);
    connect(ui->actionExit_2, &QAction::triggered, this, &NotePad::close);

    connect(ui->actionUndo, &QAction::triggered, this, &NotePad::undo);
    connect(ui->actionCopy, &QAction::triggered, this, &NotePad::copy);
    connect(ui->actionPaste, &QAction::triggered, this, &NotePad::paste);
    connect(ui->actionCut, &QAction::triggered, this, &NotePad::cut);
    connect(ui->actionDelete, &QAction::triggered, this, &NotePad::del);

    connect(ui->actionZoom_in, &QAction::triggered, this, &NotePad::fontScaleUp);
    connect(ui->actionZoom_out, &QAction::triggered, this, &NotePad::fontScaleDown);

    connect(ui->actionAuto_line, &QAction::triggered, this, &NotePad::setAutoWrap);
    connect(ui->actionFont, &QAction::triggered, this, &NotePad::selectFont);
    connect(ui->actionStatus_bar, &QAction::triggered, this, &NotePad::showStatusBar);

    connect(ui->actionRestore_the_default_zoom, &QAction::triggered, this, &NotePad::setDefaultZoom);

    connect(ui->actionReplace, &QAction::triggered, this, &NotePad::showReplaceDlg);
    connect(ui->actionFind, &QAction::triggered, this, &NotePad::showFindDlg);
    connect(ui->actionFind_next, &QAction::triggered, this, &NotePad::findNext);
    connect(ui->actionFind_Previous, &QAction::triggered, this, &NotePad::findPrev);

    ui->actionFind_next->setShortcut(Qt::Key_F3);
    ui->actionFind_Previous->setShortcut(QKeySequence(Qt::ShiftModifier | Qt::Key_F3));

    connect(ui->actionPrint_P, &QAction::triggered, this, &NotePad::print);

    connect(ui->text, &QTextEdit::textChanged, this, &NotePad::textEdited);
    connect(this, &NotePad::editedChanged,&NotePad::updateTitle);
    setWindowTitle(QString("%1 - %2").arg(QFileInfo(m_filePath).baseName(),m_appName));


    QClipboard *clipboard = QApplication::clipboard();

    ui->actionPaste->setEnabled(!clipboard->text().isEmpty());
    ui->actionCopy->setEnabled(false);
    ui->actionCut->setEnabled(false);
    ui->actionDelete->setEnabled(false);
    ui->actionUndo->setEnabled(false);

    ui->actionAuto_line->setCheckable(true);
    ui->actionAuto_line->setChecked(true);

    ui->actionStatus_bar->setCheckable(true);
    ui->actionStatus_bar->setChecked(true);

    connect(clipboard, &QClipboard::dataChanged,this, [this, clipboard]() {
        // 根据粘贴板内容的改变动态设置按钮的禁用状态
        ui->actionPaste->setEnabled(!clipboard->text().isEmpty());
    });

    connect(ui->text, &QTextEdit::copyAvailable,this,&NotePad::editAble);

    connect(ui->text, &QTextEdit::undoAvailable,this,&NotePad::undoAble);



    QSpacerItem *spacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    ui->statusbar->layout()->addItem(spacer);
    m_scale = new QLabel(QString::fromLocal8Bit("100%"),this);
    m_scale->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_cursorText = new QLabel(tr("line")+QString(" 1")+tr("column")+QString(" 1"),this);
    m_cursorText->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    ui->statusbar->addPermanentWidget(m_cursorText);
    ui->statusbar->addPermanentWidget(m_scale);

    connect(ui->text, &QTextEdit::cursorPositionChanged,this, &NotePad::cursorChanged);

    setFindReplaceDlg();


}

NotePad::~NotePad()
{
    delete ui;
    delete m_cursorText;
    delete m_scale;
    delete m_replace;
    delete m_find;
}
void NotePad::newFile(){
    if(!ui->text->toPlainText().isEmpty()){
        QMessageBox::StandardButton status = showSaveDialog();
        if (status == QMessageBox::Yes){
            bool saveStatus = saveFile();
            if(!saveStatus){
                return;
            }
        }else if(status == QMessageBox::Cancel){
            return;
        }
    }
    m_filePath = QString("%1.txt").arg(tr("untitled"));
    ui->text->setText("");
    setEdited(false);
}
void NotePad::openFile(){

    if(m_edited){
        QMessageBox::StandardButton status = showSaveDialog();
        if (status == QMessageBox::Yes){
            saveFile();
        }else if(status == QMessageBox::Cancel){
            return;
        }
    }

    // 打开文件对话框
    m_filePath = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("*.txt ;; All file (*.*)"));

    // 检查文件路径是否为空
    if (!m_filePath.isEmpty())
    {
        QFile file(m_filePath);

        // 检查文件是否成功打开
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&file);

            // 读取文件内容
            QString fileContent = in.readAll();

            // 在这里处理文件内容，例如显示在文本编辑器中
            ui->text->setText(fileContent);

            file.close();
        }
    }
}

bool NotePad::save(){
    if (!m_filePath.isEmpty())
    {
        QFile file(m_filePath);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            // 假设您要保存的内容是字符串
            QString content = ui->text->toPlainText();

            QTextStream out(&file);

            // 写入文件内容
            out << content;
            setEdited(false);

            file.close();
            return true;
        }
    }
    return false;
}

QMessageBox::StandardButton NotePad::showSaveDialog(){

    QMessageBox msgBox;
    QFileInfo fileInfo(m_filePath);
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setWindowTitle(m_appName);
    msgBox.setText(QString("%1 %3 %2").arg(tr("Do you want to save your changes to"),tr("?"),fileInfo.baseName()));

    QPushButton *yesButton = msgBox.addButton(QMessageBox::Yes);
    QPushButton *noButton = msgBox.addButton(QMessageBox::No);
    QPushButton *cancelButton = msgBox.addButton(QMessageBox::Cancel);

    yesButton->setText(tr("Save(Y)"));
    yesButton->setShortcut(Qt::Key_Y);
    noButton->setText(tr("Dont't save(N)"));
    noButton->setShortcut(Qt::Key_N);
    cancelButton->setText(tr("Cancel"));

    msgBox.exec();

    if (msgBox.clickedButton() == yesButton) {
        return QMessageBox::Yes;
    } else if (msgBox.clickedButton() == noButton) {
        return QMessageBox::No;
    }else{
        return QMessageBox::Cancel;
    }

}

void NotePad::closeEvent(QCloseEvent *event){

    QWidget::closeEvent(event);
    if(m_edited){
        QMessageBox::StandardButton status = showSaveDialog();
        if (status == QMessageBox::Yes){
            bool status = saveFile();
            if (status){
                event->accept();
            }else{
                event->ignore();
            }
        }else if(status == QMessageBox::Cancel){
            event->ignore();
        }else if(status == QMessageBox::No){
            event->accept();
        }
    }


}



bool NotePad::saveFile(){
    QFileInfo fileInfo(m_filePath);
    if (fileInfo.isRelative()){
        return saveAs();
    }else{
        return save();
    }
}

bool NotePad::saveAs(){
    QFileInfo fileInfo(m_filePath);
    QString originPath = m_filePath;
    m_filePath = QFileDialog::getSaveFileName(this,tr("Save as"), fileInfo.baseName().append(".txt"), QString("%1 (*.txt)").arg(tr("Text file")));
    if (m_filePath.isEmpty()){
        m_filePath = originPath;
    }else{
        return save();
    }
    return false;
}

void NotePad::textEdited(){
    setEdited(true);
    setFindReplaceDlg();
}

void NotePad::setFindReplaceDlg(){
    ui->actionFind->setDisabled(ui->text->toPlainText().isEmpty());
    ui->actionFind_next->setDisabled(ui->text->toPlainText().isEmpty());
    ui->actionFind_Previous->setDisabled(ui->text->toPlainText().isEmpty());
    ui->actionReplace->setDisabled(ui->text->toPlainText().isEmpty());
    if(!ui->text->toPlainText().isEmpty()){
        m_find->setFindText(m_findText);
        m_find->setContent(ui->text->toPlainText());
        m_replace->setReplaceText(m_findText);
        m_replace->setContent(ui->text->toPlainText());
    }
}

void NotePad::updateTitle(bool edited){
    QFileInfo fileInfo(m_filePath);

    if (edited){
        setWindowTitle(QString("*%1 - %2").arg(fileInfo.baseName(),m_appName));
    }else{
        setWindowTitle(QString("%1 - %2").arg(fileInfo.baseName(),m_appName));
    }
}

void NotePad::undo(){
    ui->text->undo();
    undoAble();
}

void NotePad::copy(){
    ui->text->copy();
}

void NotePad::paste(){
    ui->text->paste();
}

void NotePad::cut(){
    ui->text->cut();
}
void NotePad::setFont(QFont font){
    ui->text->setFont(font);
}
void NotePad::fontScaleUp(){
    QFont font = ui->text->font();
    int originSize = font.pointSize();
    originSize++;
    font.setPointSize(originSize);
    setFont(font);
    scaleChanged();
}

void NotePad::setAutoWrap(){
    m_autoWrap = !m_autoWrap;
    ui->text->setLineWrapMode(m_autoWrap?QTextEdit::WidgetWidth:QTextEdit::NoWrap);
}

void NotePad::fontScaleDown(){
    QFont font = ui->text->font();
    int originSize = font.pointSize();
    if (originSize>1){
        originSize--;
    }
    font.setPointSize(originSize);
    setFont(font);
    scaleChanged();
}

void NotePad::del(){
    QTextCursor cursor = ui->text->textCursor();
    if (cursor.hasSelection()) {
        cursor.deleteChar();
        ui->text->setTextCursor(cursor);
    }
}

void NotePad::editAble(){
    ui->actionCopy->setEnabled(ui->text->textCursor().hasSelection());
    ui->actionCut->setEnabled(ui->text->textCursor().hasSelection());
    ui->actionDelete->setEnabled(ui->text->textCursor().hasSelection());
}

void NotePad::undoAble(){
    ui->actionUndo->setEnabled(ui->text->isUndoRedoEnabled());
}

void NotePad::cursorChanged(){

  QTextCursor cursor = ui->text->textCursor();
  QString cursorText = tr("line")+QString(" %1").arg(cursor.blockNumber()+1)+tr(",")+tr("column")+QString(" %1").arg(cursor.columnNumber()+1);
  m_cursorText->setText(cursorText);
}

void NotePad::scaleChanged(){

  QFont font = ui->text->font();
  QString scaleText("%1%");
  m_scale->setText(scaleText.arg(font.pointSize()*100/12));
}


void NotePad::loadStyle(const QString &qssFile)
{

    //加载样式表
    QString qss;
    QFile file(qssFile);
    if (file.open(QFile::ReadOnly)) {
        //用QTextStream读取样式文件不用区分文件编码 带bom也行
        QStringList list;
        QTextStream in(&file);
        //in.setCodec("utf-8");
        while (!in.atEnd()) {
            QString line;
            in >> line;
            list << line;
        }

        file.close();
        qss = list.join("\n");
        QString paletteColor = qss.mid(20, 7);
        qApp->setPalette(QPalette(paletteColor));
        //用时主要在下面这句
        qApp->setStyleSheet(qss);
    }

}


void NotePad::selectFont(){
    QFont font = QFontDialog::getFont(nullptr);
    if (font != QFont()) {
        m_font = font;
        ui->text->setFont(m_font);
        scaleChanged();
    }
}

void NotePad::showStatusBar(){
    m_statusBar = !m_statusBar;
    if(m_statusBar){
        statusBar()->show();
    }else{
        statusBar()->hide();
    }
}

void NotePad::setDefaultZoom(){
    QFont font = ui->text->font();
    font.setPointSize(12);
    setFont(font);
    scaleChanged();
}

void NotePad::showReplaceDlg(){
    m_findText = ui->text->textCursor().selectedText();
    m_replace->setReplaceText(m_findText);
    m_replace->show();
    if(m_find->isVisible()){
        m_find->hide();
    }
}

void NotePad::showFindDlg(){
    m_findText = ui->text->textCursor().selectedText();
    m_find->setFindText(m_findText);
    m_find->show();
    if(m_replace->isVisible()){
        m_replace->hide();
    }
}

void NotePad::replace(const QString &text,const QString &replaceText,bool down,bool caseSensitive,bool cycle,bool all){
    QTextDocument::FindFlags flag;

    if (caseSensitive){
        flag |= QTextDocument::FindCaseSensitively;
    }

    if (!down){
        flag |= QTextDocument::FindBackward;
    }

    if(all){
        // replace all
        QString content = ui->text->toPlainText();
        content.replace(text,replaceText,caseSensitive?Qt::CaseSensitive:Qt::CaseInsensitive);
        ui->text->setPlainText(content);
    }else{
        // replace one
        QTextCursor cursor =ui->text->textCursor();

        if (!cursor.isNull()){
            cursor.beginEditBlock();
            cursor.insertText(replaceText);
            cursor.endEditBlock();
            find(text,down,caseSensitive,cycle);
        }
    }
}

void NotePad::print()
{
    // 创建一个打印机对象
    QPrinter printer;

    // 创建一个打印对话框，并获取用户的打印设置
    QPrintDialog dialog(&printer);
    if (dialog.exec() != QDialog::Accepted) {
        QMessageBox::warning(this,tr("NotePad"),tr("Operation has been canceled by the user."));
        return;
    }

    // 设置打印机对象的输出格式和输出设备
    printer.setOutputFormat(QPrinter::NativeFormat);

    // 执行打印操作
    ui->text->print(&printer);
}

void NotePad::find(const QString &text,bool down,bool caseSensitive,bool cycle){

    QTextDocument::FindFlags flag;
    if (caseSensitive){
        flag |= QTextDocument::FindCaseSensitively;
    }

    if (!down){
        flag |= QTextDocument::FindBackward;
    }

    if(ui->text->find(text,flag))
    {
        // 查找到后高亮显示
        QPalette palette = ui->text->palette();
        palette.setColor(QPalette::Highlight,palette.color(QPalette::Active,QPalette::Highlight));
        ui->text->setPalette(palette);
    }
    else
    {
        if(cycle){
            // 循环查找
            ui->text->moveCursor(down ? QTextCursor::Start : QTextCursor::End);
            if(ui->text->find(text,flag))
            {
                // 查找到后高亮显示
                QPalette palette = ui->text->palette();
                palette.setColor(QPalette::Highlight,palette.color(QPalette::Active,QPalette::Highlight));
                ui->text->setPalette(palette);
            }else{
                QMessageBox::information(this,tr("NotePad"),QString("%1 \"%2\"").arg(tr("Can not find"),text));
            }
        }else{
           QMessageBox::information(this,tr("NotePad"),QString("%1 \"%2\"").arg(tr("Can not find"),text));
        }

    }
}
void NotePad::findNext(){
    find(ui->text->textCursor().selectedText(),true,false,false);
}
void NotePad::findPrev(){
    find(ui->text->textCursor().selectedText(),false,false,false);
}

void NotePad::selectWord(const QString &word, int index,bool down){
    QString tmpStr = ui->text->toPlainText().mid(index,word.length());

    // 位置在范围内才设置选中
    if (tmpStr == word && index <= ui->text->toPlainText().length() - word.length()){
        // 创建文本光标
        QTextCursor cursor = ui->text->textCursor();
        // 移动光标到指定单词的位置
        cursor.setPosition(index);
        cursor.setPosition(index + word.length(), QTextCursor::KeepAnchor);

        // 设置选中的文本范围
        ui->text->setTextCursor(cursor);
    }
}
