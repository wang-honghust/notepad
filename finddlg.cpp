#include "finddlg.h"
#include "ui_finddlg.h"

FindDlg::FindDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FindDlg)
{
    ui->setupUi(this);
    ui->down->setChecked(true);
    connect(ui->nextBtn, &QPushButton::clicked, this, &FindDlg::findNext);

}

FindDlg::~FindDlg()
{
    delete ui;

}

void FindDlg::on_pushButton_2_clicked()
{
    hide();
}

void FindDlg::setFindText(QString text){
    findText = text;

    on_findText_textChanged(text);

    ui->findText->setText(findText);
    ui->findText->setFocus();
    ui->findText->selectAll();
}
void FindDlg::findNext(){
    findText = ui->findText->text();
    emit valueChanged(findText,ui->down->isChecked(),ui->caseSensitive->isChecked(),ui->cycle->isChecked());
}

void FindDlg::on_findText_textChanged(const QString &arg1)
{
    if(arg1.isEmpty()){
        ui->nextBtn->setDisabled(true);
    }else{
        ui->nextBtn->setDisabled(false);
    }
}

