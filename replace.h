#ifndef REPLACE_H
#define REPLACE_H

#include <QDialog>

namespace Ui {
class Replace;
}

class Replace : public QDialog
{
    Q_OBJECT

public:
    explicit Replace(QWidget *parent = nullptr);
    ~Replace();

    void setContent(const QString &text){
        content = text;
    };
    void setReplaceText(const QString &text);

private slots:
    void on_pushButton_4_clicked();

    void on_replaceText_textChanged(const QString &arg1);

    void on_findNextBtn_clicked();

    void on_replaceBtn_clicked();

    void on_replaceAllBtn_clicked();

signals:
    void valueChanged(const QString &text,bool down,bool caseSensitive,bool cycle);
    void replace(const QString &text,const QString &replaceText,bool down,bool caseSensitive,bool cycle,bool all);

private:
    Ui::Replace *ui;
    QString content;
    QString text;
};

#endif // REPLACE_H
