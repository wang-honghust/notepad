[English](./README.en.md) | 简体中文
# 记事本

#### 介绍
使用QT仿造windows的记事本软件

#### 展示
- 主界面
![主界面](img/Snipaste_2023-07-11_17-59-40.png)
- 查找
![查找](img/Snipaste_2023-07-11_18-00-19.png)
- 替换
![替换](img/Snipaste_2023-07-11_18-00-29.png)
- 打印
![打印](img/Snipaste_2023-07-11_18-01-02.png)
- 字体
![字体](img/Snipaste_2023-07-11_18-01-18.png)